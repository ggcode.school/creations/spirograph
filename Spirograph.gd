extends Node2D


# https://ggcode.school


var screen_size = OS.get_screen_size()


var circle_color := Color.aquamarine
var radius_color := Color.greenyellow
var point_color  := Color.pink
var spiro_color  := Color.plum
var max_spiro_points := 6000


var phi = (sqrt(5) + 1) / 2.0 - 1.0


var circles = [
	
	{
		'radius': 150.0,        # pixels
		'rotation_speed': 0.4,  # turns/s
	},
	{
		'radius': 150 * phi,          # pixels
		'rotation_speed': 0.25,  # turns/s
	},
	{
		'radius': 150 * phi * phi,          # pixels
		'rotation_speed': 0.30,  # turns/s
	},
	{
		'radius': 150 * phi * phi * phi,          # pixels
		'rotation_speed': .60,  # turns/s
	},
	
	# …
]


var dynamic_angle := 0.0
var spiro_points := Array()


func _draw():
	var viewport_size = get_viewport().get_visible_rect().size
	var center = viewport_size / 2.0
	
	draw_point(center, 2.0, point_color)
	
	for circle in self.circles:
		
		var previous_center = center
		
		draw_circle_not_disk(
			center,
			circle['radius'],
			circle_color
		)
		
		center = get_point_on_circle(
			center,
			circle['radius'],
			dynamic_angle * circle['rotation_speed']
		)
		
		draw_line(previous_center, center, radius_color)
		draw_point(center, 2.0, point_color)
	
	var spiro_size = spiro_points.size()
	
	if spiro_size < max_spiro_points:
		spiro_points.append(center)
	else:
		spiro_color = Color.blanchedalmond
	
	if spiro_size > 1:  # draw_polyline will raise an error otherwise
		draw_polyline(spiro_points, spiro_color)


func _process(_delta):
	dynamic_angle += TAU * 0.016
	update()


func get_point_on_circle(center, radius, angle):
	return center + radius * Vector2(cos(angle), sin(angle))


func draw_circle_not_disk(center, radius, color=Color.white):
	draw_arc(center, radius, 0, TAU, 42, color)


func draw_point(at_position, radius=2.0, color=Color.white):
	draw_circle(at_position, radius, color)









